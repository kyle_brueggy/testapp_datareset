package api.controller;

import api.models.InvoiceResource;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RequestMapping(value = "/data/")
@RestController
@CrossOrigin
public class DataResetController {

    @RequestMapping(value = "/reset", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity resetData() {

        Jedis jedis = new Jedis("localhost", 6379);
        jedis.connect();
        if (!jedis.ping().equals("PONG")) {
            return new ResponseEntity("Unable to get connection to database.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        //Delete all of the keys.
        Set<String> allKeys = jedis.keys("*");
        for (String key : allKeys) {
            jedis.del(key);
        }

        //Restore the data.
        List<InvoiceResource> originalData = getOriginalData();
        Gson gson = new Gson();
        for (InvoiceResource inv : originalData) {
            String invGson = gson.toJson(inv);
            jedis.set("invoiceId:" + inv.getInvoiceNo(), invGson);
        }
        jedis.close();

        return new ResponseEntity("Success", HttpStatus.OK);
    }

    private List<InvoiceResource> getOriginalData() {
        ArrayList<InvoiceResource> originalData = new ArrayList<InvoiceResource>();
        InvoiceResource inv1 = new InvoiceResource();
        inv1.setInvoiceNo("10049");
        inv1.setCompanyName("Comfort Air");
        inv1.setStatus("Sent");
        inv1.setTypeOfWork("Mechanical");
        inv1.setPrice("41.23");
        inv1.setComment("Fixed a light bulb.");
        inv1.setDueDate("2016-05-01");

        InvoiceResource inv2 = new InvoiceResource();
        inv2.setInvoiceNo("10050");
        inv2.setCompanyName("John Smith Plumbing");
        inv2.setStatus("Paid");
        inv2.setTypeOfWork("Plumbing");
        inv2.setPrice("75.00");
        inv2.setComment("Installed new toilet");
        inv2.setDueDate("2016-08-09");

        InvoiceResource inv3 = new InvoiceResource();
        inv3.setInvoiceNo("10051");
        inv3.setCompanyName("General Construction");
        inv3.setStatus("Draft");
        inv3.setTypeOfWork("Construction");
        inv3.setPrice("215.00");
        inv3.setComment("Installed new door frame.");
        inv3.setDueDate("2016-04-11");

        InvoiceResource inv4 = new InvoiceResource();
        inv4.setInvoiceNo("10052");
        inv4.setCompanyName("John Smith Plumbing");
        inv4.setStatus("Past Due");
        inv4.setTypeOfWork("Plumbing");
        inv4.setPrice("25.00");
        inv4.setComment("Fixed leaky faucet");
        inv4.setDueDate("2016-01-02");

        originalData.add(inv1);
        originalData.add(inv2);
        originalData.add(inv3);
        originalData.add(inv4);

        return originalData;
    }
}
